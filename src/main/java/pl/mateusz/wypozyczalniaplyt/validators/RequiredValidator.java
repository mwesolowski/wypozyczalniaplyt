package pl.mateusz.wypozyczalniaplyt.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@FacesValidator(value="requiredValidator")
public class RequiredValidator implements Validator{

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {
			Logging.logInfo("running requiredValidator");
		
		if (arg2 == null || "".equals(arg2.toString().trim())) {
			
			FacesMessage message = new FacesMessage();
			String messageStr = (String) arg1.getAttributes().get("messages");
			String fieldLabel = (String) arg1.getAttributes().get("label");
			
			if (messageStr == null) {
				messageStr = fieldLabel + ": " + "Validation Error: Value is required.";
			}
			
		
			message.setDetail(messageStr);
			message.setSummary(messageStr);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			
			throw new ValidatorException(message);
		}
		
	}

}
