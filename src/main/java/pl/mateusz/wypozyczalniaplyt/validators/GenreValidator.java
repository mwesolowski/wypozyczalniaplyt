package pl.mateusz.wypozyczalniaplyt.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@FacesValidator(value="genreValidator")
public class GenreValidator implements Validator{

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {
		Logging.logInfo("running GenreValidator");
		
		String genreName = ((Genre) arg2).getName().trim();
		
		if (checkIfExists(genreName)) {
			FacesMessage message = new FacesMessage();
	        message.setSeverity(FacesMessage.SEVERITY_ERROR);
	        message.setSummary("Gatunek już istnieje w bazie.");
	        message.setDetail("Gatunek już istnieje w bazie.");
	        throw new ValidatorException(message);
		}
	}

	private boolean checkIfExists(String genreName) {
		Genre g = GenreService.getGenreByName(genreName);
		
		if (g == null) {
			return false;
		} else {
			return true;
		}
	}
}
