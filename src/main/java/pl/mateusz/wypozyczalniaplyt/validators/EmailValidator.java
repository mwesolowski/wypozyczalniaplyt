package pl.mateusz.wypozyczalniaplyt.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;

import pl.mateusz.wypozyczalniaplyt.domains.Member;
import pl.mateusz.wypozyczalniaplyt.services.MemberService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@FacesValidator(value="emailValidator")
public class EmailValidator implements Validator{
	
	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		Logging.logInfo("running EmailValidator");
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		
		String email = ((String) value).trim();
		matcher = pattern.matcher(email);
		
		if(!matcher.matches()) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Nieprawidłowy adres e-mail.");
            message.setDetail("Nieprawidłowy adres e-mail.");
            throw new ValidatorException(message);
        } else {
        	checkIfExists(email);
        }
		
	}
	
	private void checkIfExists(String email) {
		
		FacesContext fc = FacesContext.getCurrentInstance();
		String currentEmail = fc.getExternalContext().getRequestParameterMap().get("email");
		
		System.out.println(currentEmail);
		// jesli nie zmieniamy maila przy edycji, pomijamy sprawdzenie czy nowy email zostal juz dopisany w bazie
		if ( (currentEmail == null) || (!currentEmail.equals(email))) {
			Member m = MemberService.findMemberEmail(email);
			
			if (m != null) {
				FacesMessage message = new FacesMessage();
	            message.setSeverity(FacesMessage.SEVERITY_ERROR);
	            message.setSummary("Podany email już istnieje w bazie.");
	            message.setDetail("Podany email już istnieje w bazie.");
	            throw new ValidatorException(message);
			}
		}
		
		
	}

}
