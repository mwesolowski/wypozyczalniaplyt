package pl.mateusz.wypozyczalniaplyt.domains;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;


@Entity
@Table(name="genre")
public class Genre implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id", unique = true, nullable = false)
	@GeneratedValue
	private int id;
	
	@Column(name="name", unique=true, nullable=false)
	private String name;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "genres")
	private Set<Movie> movies = new HashSet<Movie>(0);
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Genre() {
		// TODO Auto-generated constructor stub
	}
	
	public String toString() {
		if (id == 0) {
			return name;
		} else {
			return Integer.toString(id);
		}
		
	}

	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}

}
