package pl.mateusz.wypozyczalniaplyt.domains;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="rents")
@AssociationOverrides( {
	@AssociationOverride(name="pk.member",
			joinColumns = @JoinColumn(name = "id_mem")),
	@AssociationOverride(name="pk.movie",
			joinColumns = @JoinColumn(name = "id_mov"))
})
public class Rent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RentId pk = new RentId();
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_wyp", nullable = false)
	private Date data_wyp;
	
	@Temporal(TemporalType.DATE)
	@Column(name="data_zw", nullable = true)
	private Date data_zw = null;
	
	public Rent() {
		
	}
	
	@EmbeddedId
	public RentId getPk() {
		return pk;
	}
	public void setPk(RentId pk) {
		this.pk = pk;
	}
	
	@Transient
	public Member getMember() {
		return getPk().getMember();
	}
	
	public void setMember(Member member) {
		getPk().setMember(member);
	}
	
	@Transient
	public Movie getMovie() {
		return getPk().getMovie();
	}
	
	public void setMovie(Movie movie) {
		getPk().setMovie(movie);
	}
	
	
	public Date getData_wyp() {
		return data_wyp;
	}
	public void setData_wyp(Date data_wyp) {
		this.data_wyp = data_wyp;
	}
	

	public Date getData_zw() {
		return data_zw;
	}
	public void setData_zw(Date data_zw) {
		this.data_zw = data_zw;
	}
	
	public boolean equals(Object o) {
		if (this == o) 
			return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		Rent that = (Rent) o;
		
		if (getPk() != null ? !getPk().equals(that.getPk()) : that.getPk() != null)
			return false;
		
		return true;
	}
	
	public int hashCode() {
		return (getPk() != null ? getPk().hashCode(): 0);
	}

}
