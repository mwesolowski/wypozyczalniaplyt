package pl.mateusz.wypozyczalniaplyt.domains;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@NamedQueries({
	@NamedQuery(
			name= "findAvailableMovies",
			query = "from Movie m where m.available = true order by m.title"
			)
})
@Entity
@Table(name="movie")
public class Movie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id", unique = true, nullable = false)
	private int id;
	
	@Column(name="title", nullable = false)
	private String title;
	
	@Column(name="dvdReleaseDate", nullable = false)
	@Type(type="date")
	private Date dvdReleaseDate;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="mov_gen", joinColumns = {
			@JoinColumn(name="movie_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name="genre_id", nullable = false, updatable = false) })
	private Set<Genre> genres = new HashSet<Genre>(0);
	
	@Column(name="available")
	private boolean available = true;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.movie", cascade = CascadeType.ALL)
	private Set<Rent> rentedMovies = new HashSet<Rent>(0);

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public Date getDvdReleaseDate() {
		return dvdReleaseDate;
	}

	public void setDvdReleaseDate(Date dvdReleaseDate) {
		this.dvdReleaseDate = dvdReleaseDate;
	}

	public Set<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}

	
	public Set<Rent> getRentedMovies() {
		return rentedMovies;
	}

	public void setRentedMovies(Set<Rent> rentedMovies) {
		this.rentedMovies = rentedMovies;
	}

	

}
