package pl.mateusz.wypozyczalniaplyt.domains;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class RentId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Member member;
	private Movie movie;
	
	@ManyToOne
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	
	@ManyToOne
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	
	public boolean equals(Object o) {
		if (this == o) 
			return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		RentId that = (RentId) o;
		
		if (member != null ? !member.equals(this.member) : that.member != null)
			return false;
		
		if (movie != null ? !movie.equals(this.movie) : that.movie != null)
			return false;
		
		return true;
	}
	
	public int hashCode() {
		int result;
		
		result = (member != null ? member.hashCode() : 0);
		result = 31 * result + (movie != null ? movie.hashCode() : 0);
		
		return result;
	}
}
