package pl.mateusz.wypozyczalniaplyt.domains;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="member")
public class Member implements Serializable{
	
	public Member() {
		
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="id",unique=true,nullable=false)
	@GeneratedValue
	private int id;
	
	@Column(name="firstName",nullable=false)
	private String firstName;
	
	@Column(name="surname",nullable=false)
	private String surname;
	
	@Column(name="email",nullable=false,unique=true)
	private String email;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.member")
	private Set<Rent> rentedMovies = new HashSet<Rent>(0);
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		return Integer.toString(id) + "|" + firstName + "|" + surname + "|" + email;
	}
	
	
	public Set<Rent> getRentedMovies() {
		return rentedMovies;
	}
	public void setRentedMovies(Set<Rent> rentedMovies) {
		this.rentedMovies = rentedMovies;
	}
	

}
