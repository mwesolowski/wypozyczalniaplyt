package pl.mateusz.wypozyczalniaplyt.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {
	
	private SessionFactory sf;
	private ServiceRegistry serviceRegistry;

	private static HibernateUtil instance;
	
	public static HibernateUtil getInstance() {
		if (instance == null) {
			instance = new HibernateUtil();
		}		
		return instance;
	}
	
	private HibernateUtil() {
		
	}
	
	public void setSf(SessionFactory sf) {
		this.sf = sf;
	}
	
	public SessionFactory getSf() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			configuration.configure();

			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			setSf(configuration.buildSessionFactory(serviceRegistry));

			return sf;

			} catch (Throwable ex) {
				// Make sure you log the exception, as it might be swallowed
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
		}
	}

