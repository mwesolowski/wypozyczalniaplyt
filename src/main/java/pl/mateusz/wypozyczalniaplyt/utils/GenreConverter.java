package pl.mateusz.wypozyczalniaplyt.utils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;

@FacesConverter(value="genreConverter")
public class GenreConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		Logging.logInfo("getAsObject GenreConverter launched");
		if (arg2.trim().isEmpty()) {
			return null;
		}
		
		Genre g = new Genre();
		
		try {
			Integer id = Integer.parseInt(arg2.trim());
			g = GenreService.getGenreById(id);
			System.out.println(g.getName());
		} catch (NumberFormatException e) {
			String genreName = arg2.trim();
			g.setName(genreName);
		}
		
		
		return g;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		Logging.logInfo("getAsString GenreConverter launched");
		return arg2.toString();

	}

}
