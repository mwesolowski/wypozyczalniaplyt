package pl.mateusz.wypozyczalniaplyt.utils;

import java.util.logging.Logger;


public class Logging {

	private final static Logger LOGGER = Logger.getLogger(Logging.class.getName());

	public static Logger getLogger() {
		return LOGGER;
	}
	
	public static void logInfo(String msg) {
		LOGGER.info(msg);
	}
	
	
}
