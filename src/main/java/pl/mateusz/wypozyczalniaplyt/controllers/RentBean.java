package pl.mateusz.wypozyczalniaplyt.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import pl.mateusz.wypozyczalniaplyt.domains.Member;
import pl.mateusz.wypozyczalniaplyt.domains.Movie;
import pl.mateusz.wypozyczalniaplyt.domains.Rent;
import pl.mateusz.wypozyczalniaplyt.services.MovieService;
import pl.mateusz.wypozyczalniaplyt.services.RentService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@SessionScoped
public class RentBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Member member = new Member();
	private List<Movie> moviesToRent = new ArrayList<Movie>();
	private List<Movie> availableMoviesList = MovieService.getAvailableMovies();
	private Rent rent = new Rent();

	public void addToRentList(Movie m) {
		if(availableMoviesList.contains(m)) {
			Logging.logInfo("RentBean.addToRentList.movieList.contains() = true");
			moviesToRent.add(m);
			availableMoviesList.remove(m);
		}
	}
	
	public void saveRent() {
		Logging.logInfo("RentBean.saveRent");
		FacesContext fc = FacesContext.getCurrentInstance();
		
		boolean error = false;
		
		if (member.getId() == 0) {
			Logging.logInfo("RentBean.saveRent -> nie wybrano osoby wypozyczajacej");
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Nie wybrano osoby wypożyczającej.", null));
			error = true;
		}
		if (moviesToRent.size() == 0) {
			Logging.logInfo("RentBean.saveRent -> nie wybrano filmow do wypozyczenia");
			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Nie wybrano filmów do wypożyczenia.", null));
			error = true;
		}
		
		if (!error) {
			Logging.logInfo("RentBean.saveRent -> dodaje wypozyczenie");
			
			for (Movie m: moviesToRent) {
				
				Rent r = new Rent();
				m.setAvailable(false);
				
				r.setMember(member);
				r.setMovie(m);
				r.setData_wyp(new Date());
				r.setData_zw(null);
				
				RentService.addRent(r);
				MovieService.editMovie(m);
				MenuBean.setActivePage("/views/rent/addRentResult.xhtml");
			}
			
		}
	}
	public void removeFromRentList(Movie m) {
		Logging.logInfo("RentBean.removeFromRentList");
		moviesToRent.remove(m);
		availableMoviesList.add(m);
	}
	
	public void clearRentList() {
		availableMoviesList = MovieService.getAvailableMovies();
		moviesToRent.clear();
	}
	
	public void clearForm() {
		member = new Member();
		moviesToRent = new ArrayList<Movie>();
		availableMoviesList = MovieService.getAvailableMovies();
	}
	
	public String reset() {  
	      
		
	    
	    FacesContext fc = FacesContext.getCurrentInstance();  
	    if (fc.getPartialViewContext().isAjaxRequest()) {
	    	Logging.logInfo("RentBean - called ajax request, not reseting bean");
	    	return null;  
	    } else {
	    	Logging.logInfo("***** RESET START..."); 
	 	    if (fc.getExternalContext().getSessionMap().containsKey("rentBean")) {  
	 	      Logging.logInfo("Remove bean from session...");  
	 	      fc.getExternalContext().getSessionMap().remove("rentBean");  
	 	    }     
	 	    Logging.logInfo("Reset Components...");  
	 	    return null;
	    }
	   
 
	    
	  }  

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public List<Movie> getMoviesToRent() {
		return moviesToRent;
	}

	public void setMoviesToRent(List<Movie> moviesToRent) {
		this.moviesToRent = moviesToRent;
	}
	
	public RentBean() {
		
	}

	public List<Movie> getAvailableMoviesList() {
		return availableMoviesList;
	}

	public void setAvailableMoviesList(List<Movie> availableMoviesList) {
		this.availableMoviesList = availableMoviesList;
	}

	public Rent getRent() {
		return rent;
	}

	public void setRent(Rent rent) {
		this.rent = rent;
	}
	

}
