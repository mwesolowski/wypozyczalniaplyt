package pl.mateusz.wypozyczalniaplyt.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.domains.Movie;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;
import pl.mateusz.wypozyczalniaplyt.services.MovieService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@ViewScoped
public class EditMovieBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Movie movie = new Movie();
	private List<Movie> movieList = MovieService.getMovieList();
	private List<String> selectedGenresId; // przerobic to na arrayliste i powinno zadzialac
	
	public void processTempGenreChange(Movie m) {
		Logging.logInfo("processTempGenreChange - movie name: " + m.getTitle());
		Logging.logInfo("processTempGenreChange - genre list size: " + m.getGenres().size());
		selectedGenresId = new ArrayList<String>();
		for (Genre g: m.getGenres()) {
			selectedGenresId.add(Integer.toString(g.getId()));
		}

		
	}	
	
	public void processNewGenres(List<String> genreIds) {
		Logging.logInfo("processNewGenres - " + genreIds.size());
		
		Set<Genre> newGenres = new HashSet<Genre>();
		
		for (String s: genreIds) {
			Genre g = GenreService.getGenreById(Integer.parseInt(s));
			newGenres.add(g);
		}
		
		Logging.logInfo("processNewGenres - " + newGenres.size());
		
		movie.setGenres(newGenres);
	}

	public void editMovie(Movie m) {
		if (m.getGenres().size() == 0) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie podano gatunków.", "Nie podano gatunków."));
			RequestContext context = RequestContext.getCurrentInstance();  
	        context.addCallbackParam("validationFailed", true);    //basic parameter  
		} else {
			MovieService.editMovie(m);
		}
	}
	
	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public List<String> getSelectedGenresId() {
		return selectedGenresId;
	}

	public void setSelectedGenresId(List<String> selectedGenresId) {
		this.selectedGenresId = selectedGenresId;
	}
	
	public EditMovieBean() {
		
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

}
