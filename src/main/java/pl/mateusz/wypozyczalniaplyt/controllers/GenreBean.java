package pl.mateusz.wypozyczalniaplyt.controllers;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@RequestScoped
public class GenreBean {
			
		
	private Genre genre = new Genre();
	private Genre edited = new Genre();
	
	private List<Genre> gList;
	
	private boolean exists = false;
	private boolean genreAdded = false;

	public Genre getGenre() {
		return genre;
	}


	public void setGenre(Genre genre) {
		this.genre = genre;
	}


	public boolean isGenreAdded() {
		return genreAdded;
	}
	
	public List<Genre> getGlist() {
		gList = GenreService.getGenreList();
		return gList;
	}
	
	


	public void setgList(List<Genre> gList) {
		this.gList = gList;
	}


	public void setGenreAdded(boolean genreAdded) {
		this.genreAdded = genreAdded;
	}

	public void addGenre() {
		if (!exists) {
			GenreService.addGenre(genre);
			this.genreAdded = true;
			Logging.logInfo("adding genre: " + genre);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dodano do bazy.", "Dodano do bazy."));
		}
	}
	
	public void editGenre() {
		Logging.logInfo("editing genre");
		FacesContext fc = FacesContext.getCurrentInstance();
		
        Integer id = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("id"));
		edited.setId(id);
				
		Logging.logInfo(edited.toString());
		
		if (!exists) {
			GenreService.editGenre(edited);
		}
		
	}

	public void deleteGenre() {
		
		GenreService.deleteGenre(genre);
		FacesContext fc = FacesContext.getCurrentInstance();
		
		Integer id = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("id"));
		genre.setId(id);
		
		Logging.logInfo("deleting genre id => " + genre.getId());
		
		GenreService.deleteGenre(genre);
	}

	public Genre getEdited() {
		return edited;
	}


	public void setEdited(Genre edited) {
		this.edited = edited;
	}
	
}
