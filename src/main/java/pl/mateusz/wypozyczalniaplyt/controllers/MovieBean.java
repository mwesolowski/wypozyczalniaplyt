package pl.mateusz.wypozyczalniaplyt.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.domains.Movie;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;
import pl.mateusz.wypozyczalniaplyt.services.MovieService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@RequestScoped
public class MovieBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// TODO: przerobic edycje i dodawanie gatunkow na datatable z multiple check lista
	private Movie movie = new Movie();
	
	private List<Movie> movieList = MovieService.getMovieList();
	private List<String> selectedGenresId; // przerobic to na arrayliste i powinno zadzialac
	private Set<Genre> selectedGenres;
	
	private String[] testArray = {"1","2","3"};
	private String[] testArraySelected = {"2","1"};
	
	private boolean movieAdded = false;

	public MovieBean() {
		
	}
	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public boolean isMovieAdded() {
		return movieAdded;
	}

	public void setMovieAdded(boolean movieAdded) {
		this.movieAdded = movieAdded;
	}
	
	private Set<Genre> genreParser() {
		
		Set<Genre> movieGenreList = new HashSet<Genre>(0);
		for (int i=0; i<this.selectedGenresId.size(); i++) {
			Genre g = new Genre();
			
			int id = Integer.parseInt(this.selectedGenresId.get(i));
			g = GenreService.getGenreById(id);
			
			movieGenreList.add(g);
		}
		return movieGenreList;
	}
	public void addMovie() {
		System.out.println("addmovie");
		System.out.println("movie id: " + movie.getId());
		System.out.println("movie title: " + movie.getTitle());
		System.out.println("movie genres:");

		movie.setGenres(genreParser());
		
		if (movie.getGenres().size() == 0) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nie podano gatunków.", "Nie podano gatunków."));
		} else {
			for (Genre g: movie.getGenres()) {
				System.out.println(g.getName());
			}
			System.out.println("movie dvd release: " + movie.getDvdReleaseDate());
			MovieService.addMovie(movie);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dodano do bazy.", "Dodano do bazy."));
			
		}
		
	}
	
	public void convertIdToObject() {
		setSelectedGenres(genreParser());
	}
	
	public void populateSelectedGenres() {
		Logging.logInfo("populating genres in selected movie");
		FacesContext fc = FacesContext.getCurrentInstance();
		
		Integer idMovieToEdit = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("id"));
		Movie m = MovieService.getMovieById(idMovieToEdit);
		
		Logging.logInfo("current number of genres: " + m.getGenres().size());
	

	selectedGenresId = new ArrayList<String>();
	
		for (Genre g: movie.getGenres()) {
			selectedGenresId.add(Integer.toString(g.getId()));
		}
	}
	
	public void processTempGenreChange() {
		
		for (int i=0; i<selectedGenresId.size(); i++) {
			System.out.println(selectedGenresId.get(i));
		}
		Logging.logInfo("previous genre size: " + movie.getGenres().size());
		movie.setGenres(genreParser());
		
		Logging.logInfo("current genre size: " + movie.getGenres().size());
		
		
	}
	
	public void deleteMovie(Movie m) {
		
		MovieService.deleteMovie(m);
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

	public List<String> getSelectedGenresId() {
		Logging.logInfo("getSelectedGenresId");
		return selectedGenresId;
	}

	public void setSelectedGenresId(List<String> selectedGenresId) {
		this.selectedGenresId = selectedGenresId;
	}

	public Set<Genre> getSelectedGenres() {
		return selectedGenres;
	}

	public void setSelectedGenres(Set<Genre> selectedGenres) {
		this.selectedGenres = selectedGenres;
	}

	public String[] getTestArray() {
		return testArray;
	}

	public void setTestArray(String[] testArray) {
		this.testArray = testArray;
	}

	public String[] getTestArraySelected() {
		return testArraySelected;
	}

	public void setTestArraySelected(String[] testArraySelected) {
		this.testArraySelected = testArraySelected;
	}

	
	
}
