package pl.mateusz.wypozyczalniaplyt.controllers;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import pl.mateusz.wypozyczalniaplyt.domains.Member;
import pl.mateusz.wypozyczalniaplyt.services.GenreService;
import pl.mateusz.wypozyczalniaplyt.services.MemberService;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@RequestScoped
public class MemberBean {
	

	private Member member = new Member();	
	private Member edited = new Member();
		
	private boolean memberAdded = false;
	
	private List<Member> mlist;
	
	public Member getEdited() {
		return edited;
	}


	public void setEdited(Member edited) {
		this.edited = edited;
	}


	public Member getMember() {
		return member;
	}


	public void setMember(Member member) {
		this.member = member;
	}

	public void addMember() {
		MemberService.addMember(member);
		this.memberAdded = true;
		Logging.logInfo("adding member: " + member);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dodano do bazy.", "Dodano do bazy."));
	}
	
	public List<Member> getMemberList() {
		mlist = MemberService.getMemberList();
		return mlist;
	}


	public boolean isMemberAdded() {
		return memberAdded;
	}


	public void setMemberAdded(boolean memberAdded) {
		this.memberAdded = memberAdded;
	}




	
	public void editMember() {
		Logging.logInfo("tu je edycja ");
		FacesContext fc = FacesContext.getCurrentInstance();
		
		Integer id = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("id"));
		edited.setId(id);
				
		Logging.logInfo(edited.toString());
		
		MemberService.editMember(edited);
	}
	
	public void deleteMember() {
		
		FacesContext fc = FacesContext.getCurrentInstance();
		RequestContext rq = RequestContext.getCurrentInstance();
		Integer id = Integer.parseInt(fc.getExternalContext().getRequestParameterMap().get("id"));
		member.setId(id);
		
		if (MemberService.deleteMember(member)) {
			Logging.logInfo("deleted member of id: " + member.getId());
			rq.execute("deleteDlg.hide()");
		}

		
	}

	
}
