package pl.mateusz.wypozyczalniaplyt.controllers;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ManagedBean
@SessionScoped
public class MenuBean implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static String activePage = "/views/main/commonContent.xhtml";
	

	public String getActivePage() {
		
        return activePage;
    }

    public static void setActivePage(String newActivePage) {
    	if (activePage.equals(newActivePage)) {
    		Logging.logInfo("Choosing acitve page: " + activePage);
    	} else {
    		Logging.logInfo("Active page: " + activePage);
    		activePage = newActivePage;
    	}        
    }
    
    public void resetInput(String inputPath) {  
    	Logging.logInfo("reseting form" + inputPath);
        RequestContext.getCurrentInstance().reset(inputPath);  
    }  
}
