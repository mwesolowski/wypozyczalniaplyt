package pl.mateusz.wypozyczalniaplyt.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.mateusz.wypozyczalniaplyt.domains.Member;
import pl.mateusz.wypozyczalniaplyt.utils.HibernateUtil;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ApplicationScoped
public class MemberService {
	
	public static void addMember(Member member) {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			
		
		tx = s.beginTransaction();
		s.save(member);
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
	}


	public static List<Member> getMemberList() {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		List<Member> memberList = new ArrayList<Member>();
		try {
			
		
		tx = s.beginTransaction();
		Query q = s.createQuery("from Member");
		
		memberList = q.list();
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
		
		return memberList;
	}
	
	public static Member findMemberEmail(String email) {
		Member m = null;
		
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			
		tx = s.beginTransaction();
		Query q = s.createQuery("from Member where upper(email) = upper(:email)");
		q.setParameter("email", email.trim());

		m = (Member) q.uniqueResult();
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
		return m; 
	}
	
	public static void editMember(Member m) {
		Logging.logInfo("running: editMember");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			
		tx = s.beginTransaction();
		
		s.update(m);
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}		
	}


	public static boolean deleteMember(Member member) {
		Logging.logInfo("executing: deleteMember");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		boolean output = false;
		try {
			tx = s.beginTransaction();
			s.delete(member);
			
			tx.commit();
			output = true;
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
			
			output = false;
		} finally {
			s.close();
		}
		
		return output;
		
	}

}
