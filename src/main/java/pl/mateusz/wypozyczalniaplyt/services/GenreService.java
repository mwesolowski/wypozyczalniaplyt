package pl.mateusz.wypozyczalniaplyt.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.utils.HibernateUtil;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ApplicationScoped
public class GenreService {

	public static void addGenre(Genre g) {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		try {
			
		
		s.beginTransaction();
		s.save(g);
		
		s.getTransaction().commit();
		
		} catch (HibernateException e) {
			if (s.beginTransaction() != null) {
				s.beginTransaction().rollback();
			}
		} finally {
			s.close();
		}
		//genreList.add(g);
	}

	public static List<Genre> getGenreList() {
		Logging.logInfo("running: getGenreList");
		List<Genre> genreList = new ArrayList<Genre>();
		Session s = HibernateUtil.getInstance().getSf().openSession();
		try {
			
		
		s.beginTransaction();
		Query q = s.createQuery("from Genre");
		
		genreList = q.list();
		
		s.getTransaction().commit();
		
		} catch (HibernateException e) {
			if (s.beginTransaction() != null) {
				s.beginTransaction().rollback();
			}
		} finally {
			s.close();
		}
		return genreList;
	}

	public static void editGenre(Genre edited) {
		Logging.logInfo("running: editGenre");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			
		tx = s.beginTransaction();
		
		s.update(edited);
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}		
	}

	public static void deleteGenre(Genre g) {
		Logging.logInfo("executing: deleteGenre");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			s.delete(g);
			
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
		} finally {
			s.close();
		}
	}
	
	public static Genre getGenreByName(String name) {
		Logging.logInfo("executing: getGenreByName");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		Genre g = null;
		try {
			tx = s.beginTransaction();
			Query q = s.createQuery("from Genre where upper(name) = upper(:name)");
			q.setParameter("name", name.trim());
			
			g = (Genre) q.uniqueResult();
			
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
		} finally {
			s.close();
		}
		
		//genreList.add(g);
		return g;
		
	}

	public static Genre getGenreById(Integer id) {
		// TODO Auto-generated method stub
		Logging.logInfo("executing: getGenreById");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		Genre g = null;
		try {
			tx = s.beginTransaction();
						
			g = (Genre) s.get(Genre.class, id);
			
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
		} finally {
			s.close();
		}
		
		//genreList.add(g);
		return g;
	}
}
