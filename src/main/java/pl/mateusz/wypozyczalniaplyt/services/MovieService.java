package pl.mateusz.wypozyczalniaplyt.services;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.mateusz.wypozyczalniaplyt.domains.Genre;
import pl.mateusz.wypozyczalniaplyt.domains.Member;
import pl.mateusz.wypozyczalniaplyt.domains.Movie;
import pl.mateusz.wypozyczalniaplyt.utils.HibernateUtil;
import pl.mateusz.wypozyczalniaplyt.utils.Logging;

@ApplicationScoped
public class MovieService {
	
	public static void addMovie(Movie movie) {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		try {
			
		
		s.beginTransaction();
		s.save(movie);
		
		s.getTransaction().commit();
		
		} catch (HibernateException e) {
			if (s.beginTransaction() != null) {
				s.beginTransaction().rollback();
			}
		} finally {
			s.close();
		}
		
	}

	public static List<Movie> getMovieList() {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		List<Movie> movieList = new ArrayList<Movie>();
		try {
			
		
		tx = s.beginTransaction();
		Query q = s.createQuery("from Movie m order by m.title");
		
		movieList = q.list();
		
		// doing lazy init to populate movie collection
		if (movieList.size() > 0) {
			for (Movie m: movieList) {
				m.getGenres().iterator().next();
			}
		}
		
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
		
		return movieList;
	}

	public static Movie getMovieById(Integer idMovie) {
		// TODO Auto-generated method stub
		Logging.logInfo("executing: getMovieById");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		Movie m = null;
		try {
			tx = s.beginTransaction();
						
			m = (Movie) s.get(Movie.class, idMovie);
			
			// doing lazy init to populate movie collection
			if (!m.equals(null)) {
					m.getGenres().iterator().next();
			}
			
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
		} finally {
			s.close();
		}
		
		return m;
	}
	
	public static void editMovie(Movie m) {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			
		tx = s.beginTransaction();
		
		s.update(m);
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
		
	}

	public static void deleteMovie(Movie m) {
		
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		try {
			tx = s.beginTransaction();
			
			s.delete(m);
					
			tx.commit();
		}
		catch (HibernateException e) {
			if (tx != null) 
				tx.rollback();
		} finally {
			s.close();
		}
		
	}

	public static List<Movie> getAvailableMovies() {
		// TODO Auto-generated method stub
		Logging.logInfo("MovieService.getAvailableMovies");
		Session s = HibernateUtil.getInstance().getSf().openSession();
		Transaction tx = null;
		List<Movie> movieList = new ArrayList<Movie>();
		try {
			
		
		tx = s.beginTransaction();
		Query q = s.getNamedQuery("findAvailableMovies");
		
		movieList = q.list();
		
		// doing lazy init to populate movie collection
		if (movieList.size() > 0) {
			for (Movie m: movieList) {
				m.getGenres().iterator().next();
			}
		}
		
		
		tx.commit();
		
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			s.close();
		}
		
		return movieList;
	}
	
	
}
