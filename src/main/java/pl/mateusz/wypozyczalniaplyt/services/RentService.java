package pl.mateusz.wypozyczalniaplyt.services;

import javax.faces.bean.ApplicationScoped;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import pl.mateusz.wypozyczalniaplyt.domains.Movie;
import pl.mateusz.wypozyczalniaplyt.domains.Rent;
import pl.mateusz.wypozyczalniaplyt.utils.HibernateUtil;

@ApplicationScoped
public class RentService {

	public static void addRent(Rent rent) {
		Session s = HibernateUtil.getInstance().getSf().openSession();
		try {
			
		
		s.beginTransaction();
		
		s.save(rent);
		
		s.getTransaction().commit();
		
		} catch (HibernateException e) {
			if (s.beginTransaction() != null) {
				s.beginTransaction().rollback();
			}
		} finally {
			s.close();
		}
		
	}
}
